#!/bin/bash
docker compose down
docker compose up -d
sleep 10
docker compose exec mediawiki php ./maintenance/update.php --quick