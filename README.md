# Semantic-MediaWiki


## Install semantic mediawiki

First, open your terminal, go where you wnat to store the project and clone it :

```
git clone https://gitlab.com/barnou64/semantic_mediawiki.git
```

Dependint on your OS you need to execute the correct installation script.

## For mac/linux :

```
sh install.sh
```

## For windows :

```
install.bat
```

## Access MediaWiki

Once the script is finished, you can open your browser and mediawiki is running on localhost (port 80).

## Admin account

In this section you will find the creadentials in order to login as an administrator.

Username : 
```
admin
```
password : 
```
7DKPBqqlCyAnBjrH
```

## Be careful

If you work on your wiki and execute the install script once against it will erase all the modifications you have done on your wiki and you will have to restart from scratch.


To manage your wiki without erasing all just use the following docker compose command:

```
docker compose start
docker compose stop
```
